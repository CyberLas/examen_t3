using ANGELES_T3.Controllers;
using ANGELES_T3.Models;
using ANGELES_T3.Repositories;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using ServiceStack.Auth;

namespace ANGELES_T3_NUnit.ControllerTest
{
    public class AuthControllerTest
    {
        [Test]
        public void DebeRetornarVista_IActionResult()
        {
            var app = new Mock<ANGELES_T3.Repositories.IAuthRepository>();
            var controller = new AuthController();
            var index = controller.Index();
            Assert.IsInstanceOf<IActionResult>(index);
        }

        [Test]
        public void LoginRedireccionaraMismaPagina()
        {
            var app = new Mock<ANGELES_T3.Repositories.IAuthRepository>();
            app.Setup(o => o.FindUser(new User { Usuario = "admin", Clave = "admin" }))
                .Returns(new User { Usuario = "admin", Clave = "admin" });

            var controller = new AuthController();
            var result = controller.Index();

            Assert.IsInstanceOf<IActionResult>(result);
        }

        [Test]
        public void DebeRetornarVista_IActionSalida()
        {
            var app = new Mock<ANGELES_T3.Repositories.IAuthRepository>();
            var controller = new AuthController();
            var index = controller.Index();
            Assert.IsInstanceOf<IActionResult>(index);
        }
    }
}
using ANGELES_T3.Controllers;
using ANGELES_T3.Models;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;

namespace ANGELES_T3_NUnit.ControllerTest
{
    public class HomeControllerTest
    {
        [Test]
        public void DebeRedirigiraAuth()
        {
            var app = new Mock<ANGELES_T3.Repositories.IAuthRepository>();
            var controller = new HomeController();
            var index = controller.Index();
            Assert.IsInstanceOf<IActionResult>(index);
        }

        [Test]
        public void DebeCrearUnUsuario()
        {
            var app = new Mock<ANGELES_T3.Repositories.IAuthRepository>();
            var controller = new AuthController();
            var index = controller.Index();
            Assert.IsInstanceOf<IActionResult>(index);
        }
    }
}
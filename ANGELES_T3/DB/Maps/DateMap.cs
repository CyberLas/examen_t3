﻿using ANGELES_T3.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ANGELES_T3.DB.Maps
{
    public class DateMap : IEntityTypeConfiguration<Date>
    {
        public void Configure(EntityTypeBuilder<Date> builder)
        {
            builder.ToTable("Date");
            builder.HasKey(o => o.Code);
        }
    }
}

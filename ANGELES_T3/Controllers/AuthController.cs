﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using ANGELES_T3.Extensions;
using ANGELES_T3.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Configuration;
using ANGELES_T3.Repositories;

namespace ANGELES_T3.Controllers
{
    public class AuthController : Controller
    {

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Registro(Date date)
        {
            var context = new DB.AppContext();
            var usserLogged = HttpContext.Session.Get<User>("SessionLoggedUser");
            if (ModelState.IsValid)
            {
                var datos = new Date();
                datos.IdUser = usserLogged.Id;
                datos.Code = date.Code;
                datos.Fecha = date.Fecha;
                datos.NameMascota = date.NameMascota;
                datos.NaceMascota = date.NaceMascota;
                datos.Sexo = date.Sexo;
                datos.Especie = date.Especie;
                datos.Raza = date.Raza;
                datos.Tamano = date.Tamano;
                datos.Datos = date.DireccionDueño;
                datos.DireccionDueño = date.DireccionDueño;
                datos.Telefono = date.Telefono;
                datos.Email = date.Email;

                context.Dates.Add(date);
                context.SaveChanges();
                return RedirectToAction("Vista", "Auth");
            }
            return RedirectToAction("Index", "Auth");
        }

        [HttpGet]
        public IActionResult Vista()
        {
            var context = new DB.AppContext();
            var usserLogged = HttpContext.Session.Get<User>("SessionLoggedUser");

            ViewBag.Datos = context.Dates.Where(o => o.IdUser == usserLogged.Id).ToList();
            return View();
        }

        [HttpGet]
        public IActionResult Logout()
        {
            HttpContext.SignOutAsync();
            return RedirectToAction("Index","Home");
        }
    }
}

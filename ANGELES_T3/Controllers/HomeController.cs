﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using ANGELES_T3.Models;
using ANGELES_T3.DB;
using ANGELES_T3.Extensions;
using Microsoft.Extensions.Configuration;
using ANGELES_T3.Repositories;

namespace ANGELES_T3.Controllers
{
    public class HomeController : Controller
    {

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult CreateUser()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CreateUser(User user)
        {
            var context = new DB.AppContext();
            if (ModelState.IsValid)
            {
                var usuario = new User();
                usuario.Usuario = user.Usuario;
                usuario.Clave = user.Clave;
                context.Users.Add(usuario);
                context.SaveChanges();
                return RedirectToAction("Index", "Home");
            }
            return RedirectToAction("CreateUser", "Home");
        }

        [HttpPost]
        public IActionResult Index(User user)
        {
            var context = new DB.AppContext();
            var users = context.Users.FirstOrDefault(o => o.Usuario == user.Usuario && o.Clave == user.Clave);

            if (users == null)
                return View();

            var claims = new List<Claim>()
            {
                new Claim(ClaimTypes.Name, user.Usuario)
            };

            HttpContext.Session.Set("SessionLoggedUser", users);

            var userIdentity = new ClaimsIdentity(claims, "login");
            var principal = new ClaimsPrincipal(userIdentity);

            HttpContext.SignInAsync(principal);

            return RedirectToAction("Vista", "Auth");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ANGELES_T3.Models
{
    public class Date
    {
        public int IdUser           { get; set; }
        public int Code             { get; set; }
        public DateTime Fecha       { get; set; }
        public String NameMascota   { get; set; }
        public DateTime NaceMascota { get; set; }
        public String Sexo          { get; set; }
        public String Especie       { get; set; }
        public String Raza          { get; set; }
        public String Tamano        { get; set; }
        public String Datos         { get; set; }
        public String NameDueño     { get; set; }
        public String DireccionDueño{ get; set; }
        public String Telefono      { get; set; }
        public String Email         { get; set; }

    }
}

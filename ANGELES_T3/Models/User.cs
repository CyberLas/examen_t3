﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ANGELES_T3.Models
{
    public class User
    {
        public int Id           { get; set; }
        public String Usuario   { get; set; }
        public String Clave     { get; set; }
    }
}

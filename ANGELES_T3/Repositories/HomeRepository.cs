﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using ANGELES_T3.Models;
using ANGELES_T3.DB;
using ANGELES_T3.Extensions;
using ANGELES_T3.Controllers;
using AppContext = ANGELES_T3.DB.AppContext;

namespace ANGELES_T3.Repositories
{
    public interface IHomeRepository
    {
        public User FindUser(User user);

    }

    public class HomeRepository : IHomeRepository
    {
        private readonly AppContext context;

        public User FindUser(User user)
        {
            return context.Users.Where(o => o.Usuario == user.Usuario && o.Clave == user.Clave).FirstOrDefault();
        }

        public void SaveChanges()
        {
            var a = context.SaveChanges();
        }
    }

}

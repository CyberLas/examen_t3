﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using ANGELES_T3.Models;
using ANGELES_T3.DB;
using ANGELES_T3.Extensions;
using AppContext = ANGELES_T3.DB.AppContext;

namespace ANGELES_T3.Repositories
{
    public interface IAuthRepository
    {
        public List<Date> ListDate(int numero);
        public User FindUser(User user);
    }

    public class AuthRepository : IAuthRepository
    {
        private readonly AppContext context;

        List<Date> IAuthRepository.ListDate(int numero)
        {
            return context.Dates.Where(o => o.Code == numero).ToList();
        }

        public void SaveChanges()
        {
            var a = context.SaveChanges();
        }

        public User FindUser(User user)
        {
            return context.Users.Where(o => o.Usuario == user.Usuario && o.Clave == user.Clave).FirstOrDefault();
        }
    }
}
